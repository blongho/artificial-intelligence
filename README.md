# [Artificial intelligence for agents](https://www.miun.se/utbildning/kurser/Sok-kursplan/kursplan/?kursplanid=16757)

All laboratory exercises for the course and course projects will be versioned here. 

Each module/learning object (LO) will be in a separate directory and named appropriately


## Contribution guidelines
All suggestions and comments are welcome

Feel free to [`fork`](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) and create a [`pull request`](https://confluence.atlassian.com/bitbucket/create-a-pull-request-to-merge-your-change-774243413.html)

## Contact
Bernard Longho <lobe1602@student.miun.se> 