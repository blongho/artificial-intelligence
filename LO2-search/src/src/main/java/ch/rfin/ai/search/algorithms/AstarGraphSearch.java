/**
 * AILearningObject02
 * src.main.java.ch.rfin.ai.search.algorithms
 * AstarGraphSearch.java
 * 
 * longb
 * Feb 23, 2019
 * 
 */
package src.main.java.ch.rfin.ai.search.algorithms;

import java.util.Optional;

import ch.rfin.ai.search.Heuristic;
import ch.rfin.ai.search.InformedSearchAlgorithm;
import ch.rfin.ai.search.Node;
import ch.rfin.ai.search.Problem;

/**
 * @author longb
 *
 */
public class AstarGraphSearch<S, A> implements InformedSearchAlgorithm<S, A> {

	/**
	 * 
	 */
	public AstarGraphSearch() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see ch.rfin.ai.search.InformedSearchAlgorithm#solve(ch.rfin.ai.search.Problem, ch.rfin.ai.search.Heuristic)
	 */
	@Override
	public Optional<Node<S, A>> solve(Problem<S, A> problem, Heuristic<S> heuristic) {
		
		return null;
	}

}
