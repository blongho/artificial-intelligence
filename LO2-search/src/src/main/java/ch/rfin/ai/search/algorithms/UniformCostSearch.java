/**
 * AILearningObject02
 * src.main.java.ch.rfin.ai.search.algorithms
 * UniformCostSearch.java
 * 
 * longb
 * Feb 23, 2019
 * 
 */
package src.main.java.ch.rfin.ai.search.algorithms;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;

import ch.rfin.ai.search.Node;
import ch.rfin.ai.search.Problem;
import ch.rfin.ai.search.SearchAlgorithm;

/**
 * @author longb
 *
 */
public class UniformCostSearch<S, A> implements SearchAlgorithm<S, A> {

	/**
	 * 
	 */
	public UniformCostSearch() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see ch.rfin.ai.search.SearchAlgorithm#solve(ch.rfin.ai.search.Problem)
	 */
	@Override
	public Optional<Node<S, A>> solve(Problem<S, A> searchProblem) {
		Node<S, A> node = searchProblem.getInitialNode();
		PriorityQueue<Node<S, A>> frontier = new PriorityQueue<>();
		
		Set<Node<S, A>> explored = new HashSet<>();
		
		do {
			if(frontier.isEmpty()) {
				return Optional.empty();
			}
			
			node = frontier.remove();
			if(searchProblem.isGoal(node)) {
				return Optional.of(node);
			}
			explored.add(node);
			
			for (Node<S, A> child : searchProblem.successorsOf(node)) {
				if(!explored.contains(child) || !frontier.contains(child)) {
					frontier.add(child);
				}else {
					for (Node<S, A> node2 : frontier) {
						if(node2.compareTo(child) < 0) {
							frontier.add(child);
						}
					}
				}
			}
			
		}while(!frontier.isEmpty());
		
		return Optional.ofNullable(node);
	}

}
