/**
 * AILearningObject02
 * src.main.java.ch.rfin.ai.search.algorithms
 * BreadFirstSearch.java
 * 
 * longb
 * Feb 23, 2019
 * 
 */
package src.main.java.ch.rfin.ai.search.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;

import ch.rfin.ai.search.Node;
import ch.rfin.ai.search.Problem;
import ch.rfin.ai.search.SearchAlgorithm;
import ch.rfin.ai.search.algorithms.SearchAlgorithms;

/**
 * @author longb
 * @param <S>
 * <pre>
 * function BREADTH-FIRST-SEARCH(problem) returns a solution, or failure
 *   node &lt;- a node with STATE = problem.INITIAL-STATE, PATH-COST=0
 *   if problem.GOAL-TEST(node.STATE) then return SOLUTION(node)
 *   frontier &lt;- a FIFO queue with node as the only element
 *   explored &lt;- an empty set
 *   loop do
 *      if EMPTY?(frontier) then return failure
 *      node &lt;- POP(frontier) // chooses the shallowest node in frontier
 *      add node.STATE to explored
 *      for each action in problem.ACTIONS(node.STATE) do
 *          child &lt;- CHILD-NODE(problem, node, action)
 *          if child.STATE is not in explored or frontier then
 *              if problem.GOAL-TEST(child.STATE) then return SOLUTION(child)
 *              frontier &lt;- INSERT(child, frontier)
 * </pre>
 * 
 *
 */
public class BreadFirstGraphSearch<S, A> implements SearchAlgorithm<S, A>{

	/**
	 * 
	 */
	public BreadFirstGraphSearch() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see ch.rfin.ai.search.SearchAlgorithm#solve(ch.rfin.ai.search.Problem)
	 */
	@Override
	public Optional<Node<S, A>> solve(Problem<S, A> searchProblem) {
		Node<S, A> node = searchProblem.getInitialNode();
		if(searchProblem.isGoal(node)) return Optional.of(node);
		
		Queue<Node<S, A>> frontier = new LinkedList<>();
		frontier.add(node);
		
		Set<Node<S, A>> explored = new HashSet<>();
		
		do {
			if(frontier.isEmpty()) {
				return Optional.empty();
			}
			node = frontier.remove();
			explored.add(node);
			
			for(Node<S, A> child: searchProblem.successorsOf(node)) {
				if(!explored.contains(child.state) || !frontier.contains(child)) {
					if(searchProblem.isGoal(child)) {
						return Optional.of(child);
					}
					frontier.add(child);
				}
			}
			
		} while (!frontier.isEmpty());
		
		
		return Optional.ofNullable(node);
	}
}
